# Algortimi di ottimizzazione dei cammini su grafi 

# ALGORITMO DI DIJKSTRA:
L'algoritmo di Dijkstra richiede che sia soddisfatta la condizione dij ³ 0 per ogni arco (i,j) e permette di calcolare il cammino minimo dall' origine s (fissa) ad ogni altro vertice del grafo G.
L' algoritmo di Dijkstra può essere descritto mediante i seguenti passi:
1. si assegna ad ogni nodo i una etichetta l(i) che al termine dell' algoritmo esprime la distanza minima dall' origine s al nodo i. Si pone l(i) = +inf, per ogni "i" diverso da "s", l(s)=0. Si definisce un insieme L di nodi la cui etichetta è resa permanente, ovvero non più modificabile dall' algoritmo. Si pone L=Æ.
2. Tra i nodi i non appartenenti a L, si sceglie un nodo p per cui sia minimo il valore della etichetta, ovvero l(p)=min{l(i):i non appartenenti a L}.
Si rende p permanente L=L U {p} (unione).
3. Si aggiornano le etichette dei nodi i non permanenti raggiungibili da p, ovvero {i.i non appartenente a L, (p,i) appartenente a E }, sulla base della formula l(i) = min{l(i),l(p)+cpi }
4. Se tutti i nodi hanno etichetta permanente, ovvero se L = V, si arresta l' algoritmo. Le etichette rappresentano le lunghezze dei cammini minimi da s ad ogni altro nodo. Altrimenti si procede al passo 2.

L' operazione fondamentale dell' algoritmo di Dijkstra è costituita dalla formula di aggiornamento delle etichette, nel corso del passo 3: l(i)=min{l(i),l(p)+cij}

Questa operazione, indicata talvolta con il termine di triangolazione, ha un evidente significato intuitivo: l' etichetta di un nodo l(i) viene aggiornata allorché risulta più conveniente raggiungere i mediante un nuovo percorso, che utilizza il cammino da s a p e successivamente l' arco da p a i. E' possibile dimostrare che, quando un nodo p viene reso permanente, la sua etichetta rappresenta in modo definitivo la lunghezza del cammino minimo da s a p.
Tuttavia, la correttezza dell' operazione di triangolazione, e quindi della formula di aggiornamento delle etichette, viene pregiudicata dalla presenza di archi con distanza negativa.

# ALGORITMO DI FLOYD - WARSHALL :
Nel caso in cui esistano archi di peso negativo, l'algoritmo di Dijkstra può fornire una soluzione non ottimale.
In tali casi si può tuttavia ricorrere all'algoritmo di Floyd - Warshall, che determina i cammini minimi tra tutte le coppie di nodi del grafo G.
L'algoritmo è corretto sotto l'ipotesi che il grafo G non contenga circuiti a costo totale negativo.
L'algoritmo di Floyd - Warshall si basa sull'applicazione ripetuta dell'operazione di triangolazione.
Viene utilizzata una matrice D di dimensioni n x n, il cui generico elemento dij è destinato a rappresentare, al termine dell'algoritmo ,il costo del cammino minimo da i a j .
1. Si pone dij = cij se l'arco (i ,j) non appartiene ad E, altrimenti dij =+inf.
2. Per k =1,2,……n, esegui il passo 3.
3. Per i=1,2…..n, i ¹ k, per j=1,2,…..n, j¹k, si applica la formula di aggiornamento
dij = min{ dij , dik + dkj }. Se al termine degli aggiornamenti si ha dij < 0 per qualche nodo i, l'algoritmo si arresta poiché esistono circuiti a costo totale negativo.


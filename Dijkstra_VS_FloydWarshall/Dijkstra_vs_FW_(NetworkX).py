"""
Created on Mon Mar 16 11:57:35 2020
@author: Francesco Amori
"""



import time
import matplotlib.pyplot as plt
from networkx import nx

#------------------------------------------------------------------------------
# GENERAZIOE GRAFO RANDOM ERDOS-RENYI Metodo

n = 100  # nodes
m = 600  # edges

G = nx.gnm_random_graph(n, m)

# Propietà
for v in nx.nodes(G):
    print('%s %d %f' % (v, nx.degree(G, v), nx.clustering(G, v)))

# Stampa "adjacency list"
for line in nx.generate_adjlist(G):
    line
  # print(line)

#nx.draw(G) 
#plt.show()
#------------------------------------------------------------------------------
print('----------------------------------------------------------------------')
print('algortimo di Dijkstra _ con fibHeap_Networkx')
print ()
diz_dij = {}
start1=time.process_time()
serpe=list(G.nodes())
for node in serpe:                        
        # print(str(nx.single_source_dijkstra_path_length(G, node)))
        diz_dij[node] = nx.single_source_dijkstra_path_length(G, node)
end1=time.process_time()
print()
print('il tempo di CPU impiegato è '+ str(end1-start1))
print('----------------------------------------------------------------------')
#------------------------------------------------------------------------------
print('algortimo di FW _ implementazione_Networkx')
print()
start3=time.process_time()
diz_fw = (nx.floyd_warshall(G))
# print(diz_fw) 
end3=time.process_time()
for diz in diz_fw:
    diz_fw[diz] = {k:v for k,v in diz_fw[diz].items() if v != float('inf')}
print()
print('il tempo di CPU impiegato è '+ str(end3-start3))
print('----------------------------------------------------------------------')
#------------------------------------------------------------------------------
# cotrollo dizionari
print('CONFORNTO VALORI')
if (diz_dij==diz_fw):
    print('ok - stessi valori tra i due dizionari')
else: 
    print('le coppie chiave - valore non coincidono')

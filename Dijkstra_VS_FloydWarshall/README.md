# Dijkstra_VS_Floyd-Warshall

Required:
- Networkx (>= 2.5 (optimal = 2.7.1))
- python 3.7
- using Anaconda for setting specific libraries

------------------------------------------------------------------------------------------------------------
# DIJKSTRA
 nell'algortimo di Dijkstra un grafo con n vertici contraddistinti da numeri interi {1,2,...,n}
 e che uno di questi nodi sia quello di partenza e un altro quello di destinazione.
 avremmo un certo peso tra due nodi vicini.
 A ogni nodo devono essere associate due tag, che indicano:
 - il peso totale del cammino (la somma dei pesi sugli archi percorsi per arrivare al nodo i-esimo)
 - il nodo che precede il nodo in esame nel cammino minimo


------------------------------------------------------------------------------------------------------------
# FLOYD-WARSHALL
 L'algoritmo di FW sfrutta un processo iterativo scorrendo tutti i nodi.
 (data una matrice) nella posizione [i,j] la distanza pesata minima dal  nodo di indice i a quello j
 si attraversano solo nodi di indice minore o uguale al numero di nodi.  Se non vi è collegamento 
 allora nella cella c'è infinito. DAlla la matrice si ricava la distanza minima fra i vari nodi del grafo.
